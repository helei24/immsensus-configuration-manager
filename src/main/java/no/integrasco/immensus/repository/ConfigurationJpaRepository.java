package no.integrasco.immensus.repository;

import no.integrasco.immensus.domain.Configuration;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ConfigurationJpaRepository extends JpaRepository<Configuration, Long> {

}
