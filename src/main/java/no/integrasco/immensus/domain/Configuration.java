package no.integrasco.immensus.domain;

import javax.persistence.Column;
import javax.persistence.Entity;

@Entity(name = "configuration")
public class Configuration extends AbstractDomainObject {

  @Column
  private String configKey;

  @Column(nullable = false, length = 2047)
  private String config;

  public String getConfigKey() {

    return configKey;
  }

  public void setConfigKey(String configKey) {

    this.configKey = configKey;
  }

  public String getConfig() {

    return config;
  }

  public void setConfig(String config) {

    this.config = config;
  }
}
