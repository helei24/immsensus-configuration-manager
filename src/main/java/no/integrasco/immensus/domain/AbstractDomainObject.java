package no.integrasco.immensus.domain;

import static javax.persistence.GenerationType.AUTO;

import java.io.Serializable;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;

@MappedSuperclass
public abstract class AbstractDomainObject implements Serializable {

  private static final long serialVersionUID = 5832653249116053276L;

  @Id
  @GeneratedValue(strategy = AUTO)
  protected Long id;

  public Long getId() {

    return id;
  }

  public void setId(final Long id) {

    this.id = id;
  }

  @Override
  public Object clone() throws CloneNotSupportedException {

    return super.clone();
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }

    AbstractDomainObject that = (AbstractDomainObject) o;

    return !(id != null ? !id.equals(that.id) : that.id != null);

  }

  @Override
  public int hashCode() {
    return id != null ? id.hashCode() : 0;
  }
}
