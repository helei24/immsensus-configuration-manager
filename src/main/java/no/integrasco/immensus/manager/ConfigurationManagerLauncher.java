package no.integrasco.immensus.manager;

import static org.apache.commons.lang.StringUtils.isNotBlank;

import no.integrasco.immensus.repository.ConfigurationJpaRepository;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class ConfigurationManagerLauncher {

  private final Logger log = LoggerFactory.getLogger(this.getClass());

  private ClassPathXmlApplicationContext context = null;

  public static void main(String[] args) throws Exception {

    String pathToContextFile = getClasspathArgument(args);

    ConfigurationManagerLauncher launcher = new ConfigurationManagerLauncher();
    launcher.loadContextFromFile(pathToContextFile);

    ConfigurationProcessor processor = new ConfigurationProcessor();

    ConfigurationJpaRepository configurationJpaRepository = (ConfigurationJpaRepository)launcher.context.getBean("configurationJpaRepository");
    processor.setConfigurationJpaRepository(configurationJpaRepository);
    processor.setContext(launcher.context);
    processor.process();

  }

  public static String getClasspathArgument(final String[] args) {

    int argumentIndex = 0;
    String pathToContext = null;
    for (String arg : args) {
      argumentIndex++;
      if (arg.startsWith("-c")) {
        pathToContext = args[argumentIndex];
      }
    }
    return pathToContext;
  }

  @SuppressWarnings("unchecked")
  protected void loadContextFromFile(final String pathToContext) throws Exception {

    if (isNotBlank(pathToContext)) {
      log.info("Indexer was executed with the following input argument: {}", pathToContext);
      String contextPath = pathToContext.replace("classpath:/", "");
      log.info("Attempting to load context from: {}.", contextPath);
      try {
        context = new ClassPathXmlApplicationContext(contextPath);
      } catch (Exception e) {
        String message = String.format(
            "Unable to load context from file at classpath: '%s'", contextPath);
        throw new IllegalArgumentException(message, e);
      }
    }
    if (context == null) {
      throw new IllegalArgumentException("Requires -c and a valid classpath to launch");
    }
  }
}
