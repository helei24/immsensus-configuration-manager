package no.integrasco.immensus.manager;

import static java.lang.Class.forName;

import com.mchange.v2.c3p0.ComboPooledDataSource;
import org.springframework.beans.BeansException;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.support.JpaRepositoryFactory;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;

import java.beans.PropertyVetoException;
import java.io.IOException;
import java.util.Map;
import java.util.Properties;
import javax.persistence.EntityManager;

public class StorageBehaviourFactory {

  private ClassPathXmlApplicationContext context;

  private JpaRepository repository;

  public StorageBehaviourFactory(ClassPathXmlApplicationContext context) {
    this.context = context;
  }

  public void generator(Map<String, String> config)
      throws BeansException, PropertyVetoException, IOException, ClassNotFoundException {

    String jdbcUrl = config.get("jdbcUrl");
    String user = config.get("user");
    String password = config.get("pass");
    String domainPackage = config.get("domainPackage");
    String repositoryLocation = config.get("repositoryLocation");

    ComboPooledDataSource dataSource = new ComboPooledDataSource();
    dataSource.setUser(user);
    dataSource.setPassword(password);
    dataSource.setJdbcUrl(jdbcUrl);

    LocalContainerEntityManagerFactoryBean entityManagerFactory =
        new LocalContainerEntityManagerFactoryBean();

    Properties hibernateProperties = (Properties) context.getBean("hibernateProperties");
    HibernateJpaVendorAdapter adapter = new HibernateJpaVendorAdapter();
    adapter.setShowSql(true);
    adapter.setGenerateDdl(false);

    entityManagerFactory.setJpaVendorAdapter(adapter);
    entityManagerFactory.setDataSource(dataSource);
    entityManagerFactory.setJpaProperties(hibernateProperties);
    entityManagerFactory.setPackagesToScan(domainPackage);

    entityManagerFactory.afterPropertiesSet();
    EntityManager entityManager =
        entityManagerFactory.getNativeEntityManagerFactory().createEntityManager();

    JpaRepositoryFactory jpaRepositoryFactory = new JpaRepositoryFactory(entityManager);

    Class<JpaRepository> clazz = (Class<JpaRepository>) forName(repositoryLocation);
    repository = jpaRepositoryFactory.getRepository(clazz);

    System.out.println(repository.findAll());
  }

  public ClassPathXmlApplicationContext getContext() {

    return context;
  }

  public void setContext(ClassPathXmlApplicationContext context) {

    this.context = context;
  }

  public JpaRepository getRepository() {

    return repository;
  }

  public void setRepository(JpaRepository repository) {

    this.repository = repository;
  }
}

