package no.integrasco.immensus.manager;

import static com.google.common.collect.Maps.newHashMap;

import no.integrasco.immensus.domain.Configuration;
import no.integrasco.immensus.repository.ConfigurationJpaRepository;

import com.google.gson.Gson;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import java.beans.PropertyVetoException;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ConfigurationProcessor {

  private ConfigurationJpaRepository configurationJpaRepository;

  private ClassPathXmlApplicationContext context;

  public void process() throws PropertyVetoException, IOException, ClassNotFoundException {

    List<Configuration> configurations = configurationJpaRepository.findAll();
    Map<String, HashMap> configMap = newHashMap();
    Gson gson = new Gson();

    for (Configuration configuration : configurations) {
      configMap.put(configuration.getConfigKey(),
          gson.fromJson(configuration.getConfig(), HashMap.class));
    }

    StorageBehaviourFactory storageBehaviourFactory =
        new StorageBehaviourFactory(context);

    for (String key : configMap.keySet()) {
      storageBehaviourFactory.generator(configMap.get(key));
    }

  }

  public void setConfigurationJpaRepository(ConfigurationJpaRepository configurationJpaRepository) {

    this.configurationJpaRepository = configurationJpaRepository;
  }

  public void setContext(ClassPathXmlApplicationContext context) {

    this.context = context;
  }
}
