package no.integrasco.immensus.agent.repository;

import no.integrasco.immensus.agent.domain.Csurvey;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CSurveyRepository extends JpaRepository<Csurvey, Long> {}
