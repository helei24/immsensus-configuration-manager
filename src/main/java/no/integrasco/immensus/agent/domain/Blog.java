package no.integrasco.immensus.agent.domain;

import no.integrasco.immensus.domain.AbstractDomainObject;

import javax.persistence.Column;
import javax.persistence.Entity;

@Entity(name = "blog")
public class Blog extends AbstractDomainObject {

  @Column
  private String name;

  public String getName() {

    return name;
  }

  public void setName(String name) {

    this.name = name;
  }

  @Override
  public String toString() {

    return "Blog{" +
        "name='" + name + '\'' +
        '}';
  }
}
